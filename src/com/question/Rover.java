package com.question;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Rover {

    private static final List<String> directions = Arrays.asList("N", "E", "S", "W");
    private static final Map<Integer, Vector2D> movement = new HashMap<>();

    static {
        movement.put(0, new Vector2D(0, 1));
        movement.put(1, new Vector2D(1, 0));
        movement.put(2, new Vector2D(0, -1));
        movement.put(3, new Vector2D(-1, 0));
    }

    private Vector2D coordinates;
    private int direction;

    public Rover(int coordinateX, int coordinateY, String direction) {
        coordinates = new Vector2D(coordinateX, coordinateY);
        this.direction = directions.indexOf(direction);
    }

    public String getDescription() {
        return coordinates.getFirst() + " " + coordinates.getSecond() + " " + directions.get(Math.floorMod(direction, directions.size())) + "\n";
    }

    public void turn(char side) {
        if ('L' == side) {
            direction -= 1;
        } else if ('R' == side) {
            direction += 1;
        } else {
            throw new RuntimeException("Invalid turn direction " + side);
        }
    }

    public void move() {
        coordinates = coordinates.add(movement.get(Math.floorMod(direction, directions.size())));
    }

}
