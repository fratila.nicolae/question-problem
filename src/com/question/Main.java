package com.question;

import java.io.FileWriter;

public class Main {

    public static void main(String[] args) {
        try (Context context = new Context("data.in");
             FileWriter fileWriter = new FileWriter("result.out")) {

            while (context.hasNext()) {
                RoverEntry entry = context.readNextEntry();
                moveRover(entry.getRover(), entry.getInstructions());
                fileWriter.write(entry.getRover().getDescription());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static void moveRover(Rover rover, String instructions) {
        for (int i = 0; i < instructions.length(); i++) {
            if ('M' == instructions.charAt(i)) {
                rover.move();
            } else {
                rover.turn(instructions.charAt(i));
            }
        }
    }
}
