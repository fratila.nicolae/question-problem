package com.question;

public class Vector2D {
    private final Integer first;
    private final Integer second;

    public Vector2D(Integer first, Integer second) {
        this.first = first;
        this.second = second;
    }

    public Vector2D add(Vector2D vector) {
        return new Vector2D(this.first + vector.first, this.second + vector.second);
    }

    public Integer getFirst() {
        return first;
    }

    public Integer getSecond() {
        return second;
    }

}
