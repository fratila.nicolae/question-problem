package com.question;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Context implements Closeable {
    private final int width;
    private final int height;
    private final Scanner fileReader;

    public Context(String filename) throws FileNotFoundException {
        fileReader = new Scanner(new File(filename));
        width = fileReader.nextInt();
        height = fileReader.nextInt();
        fileReader.nextLine();
    }

    public boolean hasNext() {
        return fileReader.hasNext();
    }

    public RoverEntry readNextEntry() {
        RoverEntry entry = new RoverEntry();
        String[] roverLine = fileReader.nextLine().split(" ");
        entry.setRover(new Rover(Integer.parseInt(roverLine[0]), Integer.parseInt(roverLine[1]), roverLine[2]));
        entry.setInstructions(fileReader.nextLine());
        return entry;
    }

    @Override
    public void close() {
        fileReader.close();
    }
}
